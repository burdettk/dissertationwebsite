// getting the id of the element from the code editor user inputs in
function test(id) {
  var name = document.getElementById('code-editor2').value;
  var meta = "meta";
  if (name === meta) {                      // if the user enters this exact word, a correct alert will appear
    alert('Correct! Have a go and test your knowledge with the next tag!');
  } else {                                  // if an incorrect tag name is input, alert user they are incorrect
    alert('Incorrect! Have a read of the tag description again, and have another guess!');
  }
}
// getting the id of the element from the code editor user inputs in
function test2(id) {
  var name = document.getElementById('code-editor3').value;
  var h1 = "<h1>";
  if (name === h1) {                         // if the user enters this exact word, a correct alert will appear
    alert('Correct! Have a go and test your knowledge with the next tag!');
  } else {                                   // if an incorrect tag name is input, alert user they are incorrect
    alert('Incorrect! Have a read of the tag description again, and have another guess!');
  }
}
// getting the id of the element from the code editor user inputs in
function test3(id) {
  var name = document.getElementById('code-editor4').value;
  var ul = "<ul>";

  if (name === ul) {                        // if the user enters this exact word, a correct alert will appear
    alert('Correct! Have a go and test your knowledge with the next tag!');
  } else {                                  // if an incorrect tag name is input, alert user they are incorrect
    alert('Incorrect! Have a read of the tag description again, and have another guess!');
  }
}
// getting the id of the element from the code editor user inputs in
function test4(id) {
  var name = document.getElementById('code-editor5').value;
  var li = "<li>";

  if (name === li) {                          // if the user enters this exact word, a correct alert will appear
    alert('Correct! Have a go and test your knowledge with the next tag!');
  } else {                                    // if an incorrect tag name is input, alert user they are incorrect
    alert('Incorrect! Have a read of the tag description again, and have another guess!');
  }
}
// getting the id of the element from the code editor user inputs in
function test5(id) {
  var name = document.getElementById('code-editor6').value;
  var head = "<head>";

  if (name === head) {                        // if the user enters this exact word, a correct alert will appear
    alert('Correct! Have a go and test your knowledge with the next tag!');
  } else {                                    // if an incorrect tag name is input, alert user they are incorrect
    alert('Incorrect! Have a read of the tag description again, and have another guess!');
  }
}
// adding a score. When correct id has been inout, the score should increment by 1
var score = 0;
var scoreLabel = document.getElementById('scoreLabel');
scoreLabel.innerHTML=score;
score.innerHTML=score;
score++;
// if the id input is correct, add 1 to the score, if not, leave the score number as it is
if (id == true) {
  score = score + 1;
} else {
  score = score;
}
